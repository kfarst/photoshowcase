//
//  MessageBoxView.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

class MessageBoxView: UIView {
    var message: String! {
        willSet(text) {
            messageLabel.text = text
        }
    }
    var messageColor: UIColor! {
        didSet {
            coloredDotView.backgroundColor = messageColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .gray
        
        addSubview(messageLabel)
        addSubview(coloredDotView)
        addConstraints()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            messageLabel.pin(\UIView.topAnchor),
            messageLabel.pin(\UIView.rightAnchor),
            messageLabel.pin(\UIView.bottomAnchor),
            coloredDotView.pin(\UIView.topAnchor),
            coloredDotView.pin(\UIView.bottomAnchor),
            coloredDotView.pin(\UIView.leftAnchor),
            coloredDotView.rightAnchor.constraint(equalTo: messageLabel.leftAnchor),
            coloredDotView.widthAnchor.constraint(equalToConstant: 10)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.masksToBounds = false
        layer.shadowOffset = .zero
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
    }
    
    lazy private var messageLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.adjustsFontSizeToFitWidth = true
        l.textColor = .white
        l.font = .preferredFont(forTextStyle: .headline)
        l.textAlignment = .center
        return l
    }()
    
    lazy private var coloredDotView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
}
