//
//  PhotoListTableViewCell.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

class PhotoListTableViewCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        addConstraints()
        
        accessibilityIdentifier = String(describing: PhotoListTableViewCell.self)
        isAccessibilityElement = true
    }
    
    private func addSubviews() {
        contentView.addSubview(thumbnailImageView)
        contentView.addSubview(titleLabel)
    }

    private func addConstraints() {
        let views = [
            "thumbnailImageView": thumbnailImageView,
            "titleLabel": titleLabel
        ]
        
        let metrics = [
            "thirdWidth": bounds.width / 3
        ]
        
        
        var constraints = [NSLayoutConstraint]()
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "H:|-[thumbnailImageView]-[titleLabel]-|", options: [], metrics: metrics, views: views))
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|-[titleLabel]-|", options: [], metrics: metrics, views: views))
        constraints.append(thumbnailImageView.pin(\UIView.layoutMarginsGuide.centerYAnchor))
        constraints.append(thumbnailImageView.pin(\UIView.layoutMarginsGuide.widthAnchor, to: 100))
        constraints.append(thumbnailImageView.pin(\UIView.layoutMarginsGuide.heightAnchor, to: 100))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        thumbnailImageView.image = nil
        titleLabel.text = nil
    }
    
    var thumbnailImage: UIImage? {
        didSet {
            thumbnailImageView.alpha = 0
            thumbnailImageView.image = thumbnailImage
            
            UIView.animate(withDuration: 0.3) {
                self.thumbnailImageView.alpha = 1
            }
        }
    }
    
    lazy private(set) var titleLabel: UILabel = {
        return UILabel(textStyle: .headline, textColor: .gray)
    }()
    
    lazy private(set) var thumbnailImageView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.backgroundColor = .gray
        i.setContentHuggingPriority(.required, for: .horizontal)
        i.contentMode = .scaleAspectFill
        i.clipsToBounds = true
        return i
    }()
}
