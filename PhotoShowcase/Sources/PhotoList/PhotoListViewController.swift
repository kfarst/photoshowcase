//
//  PhotoListViewController.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright (c) 2019 Kevin Farst. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

let PAGE_LIMIT = 20

protocol PhotoListDisplayLogic: class {
    func renderPhotosList(viewModel: PhotoList.Fetch.ViewModel)
    func renderThumbnailImage(viewModel: PhotoList.Thumbnail.ViewModel)
    func showEmptyStateIfNeeded()
}

class PhotoListViewController: UITableViewController {
    var interactor: PhotoListBusinessLogic?
    var router: (NSObjectProtocol & PhotoListRoutingLogic & PhotoListDataPassing)?
    
    private(set) var isPrefetching = false
    private(set) var currentPage = 0 {
        didSet {
            if currentPage == 0 {
                currentPage += 1
            }
            
            
            let request = PhotoList.Fetch.Request(page: currentPage, limit: PAGE_LIMIT)
            interactor?.loadPhotosList(request: request)
        }
    }
    private(set) var isPrefetchingDisabled = false
    private(set) var photos = [Photo]()
    
    private(set) var viewState: ViewState = .loading {
        didSet {
            loadingActivityIndicatorView.stopAnimating()
            emptyView.isHidden = true
            tableView.separatorStyle = .none
            
            switch viewState {
            case .loaded:
                tableView.separatorStyle = .singleLine
                break
            case .loading:
                loadingActivityIndicatorView.startAnimating()
                break
            case .empty:
                emptyView.isHidden = false
                break
            }
        }
    }
    
    // MARK: Object lifecycle
    
    init(configurator: PhotoListConfigurator = PhotoListConfigurator.shared) {
        super.init(nibName: nil, bundle: nil)
        
        configurator.configure(viewController: self)
        
        tableView.register(PhotoListTableViewCell.self, forCellReuseIdentifier: String(describing: PhotoListTableViewCell.self))
        tableView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.prefetchDataSource = self
        tableView.accessibilityLabel = "Photo List"
        tableView.isAccessibilityElement = true
        
        addSubviews()
        addConstraints()
    }
    
    private func addSubviews() {
        view.addSubview(loadingActivityIndicatorView)
        view.addSubview(emptyView)
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            loadingActivityIndicatorView.pin(\UIView.layoutMarginsGuide.centerXAnchor),
            loadingActivityIndicatorView.pin(\UIView.layoutMarginsGuide.centerYAnchor),
            
            emptyView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            emptyView.rightAnchor.constraint(equalTo: view.layoutMarginsGuide.rightAnchor),
            emptyView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor),
            emptyView.leftAnchor.constraint(equalTo: view.layoutMarginsGuide.leftAnchor),
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        PhotoListConfigurator.shared.configure(viewController: self)
    }
    
    // MARK: Routing
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentPage = 1
        
        refreshControl = {
            let r = UIRefreshControl()
            r.attributedTitle = NSAttributedString(string: "Pull to Refresh", attributes: [:])
            
            let action = #selector(PhotoListViewController.refreshControlDidStart(sender:event:))
            r.addTarget(self, action: action, for: .valueChanged)
            
            return r
        }()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewState = .loading
        navigationItem.title = "Photos"
    }
    
    @objc private func refreshControlDidStart(sender: UIRefreshControl?, event: UIEvent?) {
        refreshControl?.beginRefreshing()
        tableView.reloadData()
        currentPage = 0
    }
    
    lazy private(set) var loadingActivityIndicatorView: UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.hidesWhenStopped = true
        i.style = .gray
        i.transform = CGAffineTransform(scaleX: 2, y: 2)
        i.isAccessibilityElement = true
        i.accessibilityIdentifier = "PhotoListLoadingActivityIndicatorView"
        return i
    }()
    
    lazy private(set) var emptyView: UIView = {
        let i = PhotoListEmptyView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.backgroundColor = .white
        i.isAccessibilityElement = true
        i.accessibilityIdentifier = "PhotoListEmptyView"
        return i
    }()
    
    deinit {
        interactor?.cancelAllImageDownloads()
    }
}

// MARK: UITableViewDataSource
extension PhotoListViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let photo = photos[indexPath.row]
        let request = PhotoList.Thumbnail.Request(url: photo.thumbnailUrl, cell: cell)
        interactor?.loadPhotoThumbnail(request: request)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PhotoListTableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PhotoListTableViewCell.self), for: indexPath) as? PhotoListTableViewCell else {
                return PhotoListTableViewCell()
            }
            
            return cell
        }()
        
        cell.titleLabel.text = photos[indexPath.row].title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cell = tableView.cellForRow(at: indexPath) as? PhotoListTableViewCell {
            let imgHeight = cell.thumbnailImageView.bounds.height
            let titleHeight = cell.titleLabel.bounds.height
            
            return max(max(imgHeight, titleHeight), 150)
        }
        
        return 150
    }
}

// MARK: UITableViewDataSourcePrefetching
extension PhotoListViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard !isPrefetching && !isPrefetchingDisabled else { return }
        
        let needsFetch = indexPaths.contains { $0.row >= photos.count - 4 }
        
        if needsFetch  {
            isPrefetching = true
            currentPage += 1
        }
    }
}

// MARK: PhotoListDisplayLogic
extension PhotoListViewController: PhotoListDisplayLogic {
    func renderPhotosList(viewModel: PhotoList.Fetch.ViewModel) {
        isPrefetching = false
        
        guard !(currentPage > 1 && viewModel.photos.isEmpty) else {
            isPrefetchingDisabled = true
            return
        }
        
        let lowerRange = photos.count
        
        photos.append(contentsOf: viewModel.photos)
        
        let upperRange = photos.count
        
        let indexPaths = (lowerRange..<upperRange)
            .map { IndexPath(item: $0, section: 0) }
        
        viewState = photos.isEmpty ? .empty : .loaded
        
        tableView.performBatchUpdates({
            tableView.insertRows(at: indexPaths, with: .automatic)
        }) { completed in
            self.refreshControl?.endRefreshing()
            
            guard completed else {
                self.tableView.reloadData()
                return
            }
        }
    }
    
    func renderThumbnailImage(viewModel: PhotoList.Thumbnail.ViewModel) {
        if let cell = viewModel.cell as? PhotoListTableViewCell {
            cell.thumbnailImage = viewModel.image
        }
    }
    
    func showEmptyStateIfNeeded() {
        if viewState == .loading {
            viewState = .empty
        }
    }
}
