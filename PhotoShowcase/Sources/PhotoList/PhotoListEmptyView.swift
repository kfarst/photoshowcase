//
//  PhotoListEmptyView.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 2/1/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit
import SwiftIcons

class PhotoListEmptyView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews()
        addConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let messageSize = messageView.intrinsicContentSize
        let iconSize = CGSize(width: messageSize.width * 2, height: messageSize.width * 2)
        
        iconView.setIcon(icon: .ionicons(.iosCamera), textColor: .gray, backgroundColor: .white, size: iconSize)
    }
    
    private func addSubviews() {
        addSubview(contentView)
        
        contentView.addSubview(iconView)
        contentView.addSubview(messageView)
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            contentView.pin(\UIView.centerXAnchor),
            contentView.pin(\UIView.centerYAnchor),
            
            iconView.pin(\UIView.topAnchor),
            iconView.pin(\UIView.leftAnchor),
            iconView.pin(\UIView.rightAnchor),
            
            messageView.topAnchor.constraint(greaterThanOrEqualToSystemSpacingBelow: iconView.bottomAnchor, multiplier: 1.5),
            messageView.centerXAnchor.constraint(equalTo: iconView.centerXAnchor),
            messageView.pin(\UIView.bottomAnchor),
            ])
    }
    
    lazy private var contentView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    lazy private var iconView: UIImageView = {
        let i =  UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    lazy private var messageView: UILabel = {
        let l = UILabel(textStyle: .title3, textColor: .gray)
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "No photos found"
        return l
    }()
}
