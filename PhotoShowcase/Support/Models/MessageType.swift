//
//  MessageType.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

enum MessageType {
    case success
    case error
    case info
    
    var color: UIColor {
        switch self {
        case .success: return .green
        case .error: return .red
        case .info: return .yellow
        }
    }
}
