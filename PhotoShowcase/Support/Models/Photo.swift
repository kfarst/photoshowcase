//
//  Photo.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

struct Photo: Decodable {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
    
    enum CodingKeys: String, CodingKey {
        case albumId
        case id
        case title
        case url
        case thumbnailUrl
    }
}
