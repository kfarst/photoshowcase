//
//  ViewState.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

enum ViewState {
    case loading
    case empty
    case loaded
}
