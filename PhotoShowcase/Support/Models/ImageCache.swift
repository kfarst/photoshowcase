//
//  ImageCache.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

class ImageCache: NSCache<AnyObject, AnyObject> {
    static let shared = ImageCache()
    
    private var sessionTasks: [String: URLSessionDataTask] = [:]
    
    func fetchOrDownload(forUrl url: URL, completionHandler: @escaping (UIImage?, Error?) -> Void) {
        if let imageFromCache = object(forKey: url.absoluteString as AnyObject) as? UIImage {
            completionHandler(imageFromCache, nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let response = data {
                self.sessionTasks.removeValue(forKey: url.absoluteString)
                
                DispatchQueue.main.async {
                    if let imageToCache = UIImage(data: response) {
                        self.setObject(imageToCache, forKey: url.absoluteString as AnyObject)
                        completionHandler(imageToCache, nil)
                    } else {
                        completionHandler(nil, nil)
                    }
                }
            } else if let error = error {
                completionHandler(nil, error)
            } else {
                completionHandler(nil, nil)
            }
        }
        
        sessionTasks.updateValue(task, forKey: url.absoluteString)
        
        task.resume()
    }
    
    func cancelAllDownloads() {
        sessionTasks.values.forEach { task in
            task.cancel()
        }
        
        sessionTasks.removeAll()
    }
    
    func cancelDownload(forUrlString urlString: String) {
        if let task = sessionTasks.removeValue(forKey: urlString) {
            task.cancel()
        }
    }
}
