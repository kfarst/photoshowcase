//
//  PhotoApiClient.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

class PhotoApiClient: ApiClient {
    static var shared: PhotoApiClient = {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Content-Type" : "application/json",
            "Accept" : "application/json"
        ]
        config.requestCachePolicy = .useProtocolCachePolicy
        return PhotoApiClient(configuration: config)
    }()
    
    func get(params: [String: Any], _ completion: @escaping (ApiClientResult<[Photo]>) -> Void) {
        let url = Endpoints<PhotoApiClient>.photos.url
        let request = buildRequest(.get, url: url, params: params)
        return fetchCollection(request: request, completion: completion)
    }
    
    override class var baseURL: String {
        return Bundle.main.object(forInfoDictionaryKey: "PHOTO_API_BASE_URL")  as! String
    }
}
