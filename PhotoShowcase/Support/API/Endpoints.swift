//
//  Endpoints.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

enum Endpoints<T: ApiClient> {
    case photos

    var url: URL {
        let path: String
        
        switch self {
        case .photos:
            path = "/photos"
            break;
        }
        
        return URL(string: T.baseURL + path)!
    }
}
