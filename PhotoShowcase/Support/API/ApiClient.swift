//
//  ApiClient.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

public enum ApiClientResult<T> {
    case success(T)
    case error(Error)
    case notFound
    case serverError(T)
    case clientError(T)
    case unexpectedResponse(Data)
}

public enum RequestType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

fileprivate enum CustomError: Error {
    case timeout
    case noPayload
}

class ApiClient: NSObject {
    typealias JsonTaskCompletionHandler = (Data?, HTTPURLResponse?, Error?) -> Void
    
    let configuration: URLSessionConfiguration
    
    lazy var session: URLSession = {
        return URLSession(configuration: self.configuration)
    }()
    
    lazy var decoder: JSONDecoder = {
        let d = JSONDecoder()
        d.keyDecodingStrategy = .convertFromSnakeCase
        return d
    }()
    
    var currentTasks: Set<URLSessionDataTask> = []
    
    init(configuration: URLSessionConfiguration) {
        self.configuration = configuration
    }
    
    func jsonTaskWithRequest(request: URLRequest, completion: @escaping JsonTaskCompletionHandler) -> URLSessionDataTask {
        var task: URLSessionDataTask?
        task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            self.currentTasks.remove(task!)
            if let http = response as? HTTPURLResponse {
                if let err = error {
                    completion(nil, http, err)
                } else {
                    if let data = data {
                        completion(data, http, nil)
                    } else {
                        completion(nil, http, CustomError.noPayload)
                    }
                }
            } else {
                completion(nil, nil, CustomError.timeout)
            }
        })
        currentTasks.insert(task!)
        return task!
    }
    
    func fetch<T>(request: URLRequest, parseBlock: @escaping (Data) -> T?, completion: @escaping (ApiClientResult<T>) -> Void) {
        print(request.url!.absoluteString)
        let task = jsonTaskWithRequest(request: request) { (json, response, error) in
            DispatchQueue.main.async() {
                if let err = error {
                    completion(.error(err))
                } else {
                    switch response!.statusCode {
                    case 200...299:
                        if let resource = parseBlock(json!) {
                            completion(.success(resource))
                        } else {
                            print("WARNING: Couldn't parse the following JSON as a \(T.self)")
                            print(json!)
                            completion(.unexpectedResponse(json!))
                        }
                        
                    case 404: completion(.notFound)
                    case 400...499:
                        if let resource = parseBlock(json!) {
                            completion(.clientError(resource))
                        } else {
                            print("WARNING: Couldn't parse the following JSON as a \(T.self)")
                            print(json!)
                            completion(.unexpectedResponse(json!))
                        }
                    case 500...599:
                        if let resource = parseBlock(json!) {
                            completion(.serverError(resource))
                        } else {
                            print("WARNING: Couldn't parse the following JSON as a \(T.self)")
                            print(json!)
                            completion(.unexpectedResponse(json!))
                        }
                    default:
                        print("Received HTTP \(response!.statusCode), which was not handled")
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func fetchResource<T: Decodable>(request: URLRequest, rootKey: String? = nil, completion: @escaping (ApiClientResult<T>) -> Void) {
        fetch(request: request, parseBlock: { (data: Data) -> T? in
            do {
                let results = try self.decoder.decode(T.self, from: data)
                return results
            } catch let error {
                print(error.localizedDescription)
                return nil
            }
            
        }, completion: completion)
    }
    
    func fetchCollection<T: Decodable>(request: URLRequest, rootKey: String? = nil, completion: @escaping (ApiClientResult<[T]>) -> Void) {
        fetch(request: request, parseBlock: { (data: Data) -> [T]? in
            
            do {
                let results = try self.decoder.decode([T].self, from: data)
                return results
            } catch let error {
                print(error.localizedDescription)
                return nil
            }
            
        }, completion: completion)
    }
    
    func buildRequest(_ verb: RequestType, url: URL, params: Dictionary<String, Any> = [:]) -> URLRequest {
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        let buildRequest: () -> URLRequest = {
            var r = URLRequest(url: urlComponents!.url!)
            r.httpMethod = verb.rawValue
            return r
        }
        
        var request: URLRequest
        
        if params.keys.count > 0 {
            urlComponents?.queryItems = params.toQueryItems()
        }
        
        request = buildRequest()
        
        return request
    }
    
    class var baseURL: String {
        fatalError("Please define baseURL in child class")
    }
}
