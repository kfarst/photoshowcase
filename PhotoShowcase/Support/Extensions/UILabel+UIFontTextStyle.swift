//
//  UILabel+UIFontTextStyle.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(textStyle: UIFont.TextStyle, textColor: UIColor) {
        self.init()
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.font = .preferredFont(forTextStyle: textStyle)
        self.adjustsFontForContentSizeCategory = true
        self.numberOfLines = 0
        self.textColor = textColor
    }
    
    func boundingRectForCharacterRange(range: NSRange) -> CGRect? {
        guard let attributedText = attributedText else { return nil }
        
        let textStorage = NSTextStorage(attributedString: attributedText)
        let layoutManager = NSLayoutManager()
        
        textStorage.addLayoutManager(layoutManager)
        
        let textContainer = NSTextContainer(size: intrinsicContentSize)
        
        textContainer.lineFragmentPadding = 0.0
        
        layoutManager.addTextContainer(textContainer)
        
        var glyphRange = NSRange()
        
        layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: &glyphRange)
        
        return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
    }
}
