//
//  UIView+KeyPath.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

extension UIView {
    
    func pin<LayoutAnchorType, Axis>(_ anchor: KeyPath<UIView, LayoutAnchorType>) -> NSLayoutConstraint
        where LayoutAnchorType : NSLayoutAnchor<Axis> {
            return pin(anchor, to: anchor)
    }
    
    
    func pin<LayoutAnchorType, Axis>(_ from: KeyPath<UIView, LayoutAnchorType>,
                                     to: KeyPath<UIView, LayoutAnchorType>) -> NSLayoutConstraint
        where LayoutAnchorType : NSLayoutAnchor<Axis> {
            
            guard let parent = superview else { fatalError("must addSubview first") }
            
            let source = self[keyPath: from]
            let target = parent[keyPath: to]
            return source.constraint(equalTo: target)
    }
    
    func pin(_ anchor: KeyPath<UIView, NSLayoutDimension>, to constant: CGFloat) -> NSLayoutConstraint {
        return self[keyPath: anchor].constraint(equalToConstant: constant)
    }
    
}
