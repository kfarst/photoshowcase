//
//  Dictionary+URLQueryItems.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

extension Dictionary {
    func toQueryItems() -> [URLQueryItem] {
        var items: [URLQueryItem] = [URLQueryItem]()
        
        for pair in self.enumerated() {
            items.append(URLQueryItem(name: "\(pair.element.key)", value: "\(pair.element.value)"))
        }
        
        return items
    }
}
