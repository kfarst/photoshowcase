//
//  UIViewController+Message.swift
//  PhotoShowcase
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

protocol MessageBoxProtocol {
    func showError(message: String)
    func showSuccess(message: String)
}

extension UIViewController: MessageBoxProtocol {
    func showError(message: String) {
        showMessage(withText: message, andType: .error)
    }
    
    func showSuccess(message: String) {
        showMessage(withText: message, andType: .success)
    }
    
    private func showMessage(withText text: String, andType type: MessageType) {
        DispatchQueue.main.async {
            let v = MessageBoxView()
            v.message = text
            v.messageColor = type.color
            v.alpha = 0
            (self.navigationController?.view ?? self.view).insertSubview(v, at: self.view.subviews.count)
            
            NSLayoutConstraint.activate([
                v.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10),
                v.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10),
                v.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50),
                v.heightAnchor.constraint(equalToConstant: 50)
                ])
            
            UIView.animate(withDuration: 0.5) {
                v.alpha = 1
            }
            
            Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
                UIView.animate(withDuration: 0.5, animations: {
                    v.alpha = 0
                }, completion: { (completed) in
                    if completed {
                        v.removeFromSuperview()
                    }
                })
            }
        }
    }
}
