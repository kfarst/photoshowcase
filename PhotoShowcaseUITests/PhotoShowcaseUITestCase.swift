//
//  PhotoShowcaseUITestCase.swift
//  PhotoShowcaseUITests
//
//  Created by Kevin Farst on 2/3/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import XCTest
import OHHTTPStubs

@testable import PhotoShowcase

class PhotoShowcaseUITestCase: XCTestCase {
    
    let dynamicStubs = DynamicHTTPSStubs()
    var app: XCUIApplication?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.        
        OHHTTPStubs.removeAllStubs()
        
        dynamicStubs.setUp()
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.

        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
	        OHHTTPStubs.onStubMissing { request in
            XCTFail("Missing stub for \(request)")
        }
        
        app = XCUIApplication()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        OHHTTPStubs.removeAllStubs()
    }
    
    func test_photosLoaded() {
        dynamicStubs.setupStub(test: { (request) -> Bool in
            return request.url?.path ~= "/path"
        }, filename: "PhotoListFixtures")

        app?.launch()

        let tableView = app?.tables.containing(.table, identifier: "Photo List")
        let rows = tableView?.cells.containing(.cell, identifier: "PhotoListTableViewCell")

        XCTAssertGreaterThan(rows!.count, 0, "Table view did not show photos")
    }
//    
//    func test_noPhotosLoaded() {
//        // Use recording to get started writing UI tests.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//        dynamicStubs.setupStub(test: pathStartsWith("/photos"), filename: nil, { request -> OHHTTPStubsResponse in
//            return OHHTTPStubsResponse(data: Data(base64Encoded: "[]")!, statusCode: 200, headers: [:])
//        })
//        
//        app?.launch()
//        
//        let tableView = app?.tables.containing(.table, identifier: "Photo List")
//        let rows = tableView!.cells.containing(.cell, identifier: "PhotoListTableViewCell")
//        
//        XCTAssertEqual(rows.count, 0, "Table view incorrectly loaded photos")
//        
//        let emptyView = app?.otherElements["PhotoListEmptyView"]
//        
//        XCTAssertFalse(emptyView!.isHittable, "Empty photo list view not visible")
//    }
//    
//    func test_error() {
//        dynamicStubs.setupStub(test: pathStartsWith("/photos"), filename: nil, { request -> OHHTTPStubsResponse in
//            return OHHTTPStubsResponse(error: TestResultError())
//        })
//
//        app?.launch()
//    }
}
