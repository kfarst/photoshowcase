//
//  DynamicHTTPStubs.swift
//  PhotoShowcaseUITests
//
//  Created by Kevin Farst on 2/3/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation
import OHHTTPStubs

class DynamicHTTPSStubs {
    func setUp() {
        setupInitialStubs()
    }
    
    func setupInitialStubs() {
        // Setting up all the initial mocks from the array
        for stub in initialStubs {
            setupStub(test: stub.test, filename: stub.jsonFilename, stub.response)
        }
    }
    
    public func setupStub(test: @escaping OHHTTPStubsTestBlock, filename: String?, _ response: ((URLRequest) -> OHHTTPStubsResponse)? = nil) {
        stub(condition: test) { (request) -> OHHTTPStubsResponse in
            if let path = Bundle.main.path(forResource: filename, ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    return response != nil ? response!(request) : OHHTTPStubsResponse(data: data, statusCode: 200, headers: [:])
                } catch let error {
                    return OHHTTPStubsResponse(error: error)
                }
            } else {
                return response != nil ? response!(request) : OHHTTPStubsResponse(jsonObject: "{}", statusCode: 422, headers: [:])
            }
        }
    }
}

let initialStubs = [
    HTTPStubInfo(test: pathMatches("\\.(png|jpg)"), jsonFilename: "PhotoListFixture", response: { (request) -> OHHTTPStubsResponse in
        return OHHTTPStubsResponse(data: Data(), statusCode: 200, headers: [:])
    })
]

struct HTTPStubInfo {
    let test: OHHTTPStubsTestBlock
    let jsonFilename: String
    let response: (URLRequest) -> OHHTTPStubsResponse
    
    init(test: @escaping OHHTTPStubsTestBlock, jsonFilename: String, response: @escaping (URLRequest) -> OHHTTPStubsResponse) {
        self.test = test
        self.jsonFilename = jsonFilename
        self.response = response
    }
}
