//
//  PhotoListPresenterMock.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation
@testable import PhotoShowcase

class PhotoListPresenterMock: PhotoListPresentationLogic {
    var showPhotoListCalled = false
    var showThumbnailImageCalled = false
    var showErrorCalled = false
    
    func showPhotoList(response: PhotoList.Fetch.Response) {
        showPhotoListCalled = true
    }
    
    func showThumbnailImage(response: PhotoList.Thumbnail.Response) {
        showThumbnailImageCalled = true
    }
    
    func showError(withMessage message: String) {
        showErrorCalled = false
    }
}
