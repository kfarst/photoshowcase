//
//  PhotoListInteractorMock.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 2/1/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

@testable import PhotoShowcase

class PhotoListInteractorMock: PhotoListBusinessLogic {
    var loadPhotosListCalled = false
    var loadPhotoThumbnailCalled = false
    var cancelAllImageDownloadsCalled = false
    var cancelImageDownloadCalled = false
    
    func loadPhotosList(request: PhotoList.Fetch.Request) {
        loadPhotosListCalled = true
    }
    
    func loadPhotoThumbnail(request: PhotoList.Thumbnail.Request) {
        loadPhotoThumbnailCalled = true
    }
    
    func cancelAllImageDownloads() {
        cancelAllImageDownloadsCalled = true
    }
    
    func cancelImageDownload(request: PhotoList.CancelImgDownload.Request) {
        cancelImageDownloadCalled = true
    }
    
}
