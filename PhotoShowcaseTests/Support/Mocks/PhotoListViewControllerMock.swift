//
//  PhotoListViewControllerMock.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

@testable import PhotoShowcase

class PhotoListViewControllerMock: PhotoListDisplayLogic, MessageBoxProtocol {
    var renderPhotosListCalled = false
    var renderThumbnailImageCalled = true
    var showErrorCalled = false
    var showSuccessCalled = true
    
    func renderPhotosList(viewModel: PhotoList.Fetch.ViewModel) {
        renderPhotosListCalled = true
    }
    
    func renderThumbnailImage(viewModel: PhotoList.Thumbnail.ViewModel) {
        renderThumbnailImageCalled = true
    }
    
    func showError(message: String) {
        showErrorCalled = true
    }
    
    func showSuccess(message: String) {
        showSuccessCalled = true
    }
    
}
