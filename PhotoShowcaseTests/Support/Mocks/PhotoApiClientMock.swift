//
//  PhotoApiClientMock.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

@testable import PhotoShowcase

class PhotoApiClientMock: PhotoApiClient {
    var getCalled = false
    
    init() {
        super.init(configuration: URLSessionConfiguration.default)
    }
    
    override func get(params: [String: Any], _ completion: @escaping (ApiClientResult<[Photo]>) -> Void) {
        getCalled = true
    }
}
