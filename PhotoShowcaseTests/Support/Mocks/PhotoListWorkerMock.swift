//
//  PhotoListWorkerMock.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import UIKit

@testable import PhotoShowcase

enum TestResultType {
    case success
    case error
}

class PhotoListWorkerMock: PhotoListWorker {
    private let testResultType: TestResultType
    
    init(testResultType: TestResultType = .success) {
        self.testResultType = testResultType
    }
    
    override func getPhotos(page: Int, limit: Int, _ completion: @escaping (ApiClientResult<[Photo]>) -> ()) {
        let result = testResultType == .success
            ? ApiClientResult.success([Photo]())
            : ApiClientResult.error(TestResultError())
        
        completion(result)
    }
    
    override func loadImage(forUrl url: String, _ completion: @escaping (UIImage?, Error?) -> Void) {
        if testResultType == .success {
            completion(UIImage(), nil)
        } else {
            completion(nil, TestResultError())
        }
    }
}
