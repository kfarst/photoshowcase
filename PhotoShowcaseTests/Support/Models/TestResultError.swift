//
//  TestResultError.swift
//  PhotoShowcaseUITests
//
//  Created by Kevin Farst on 2/3/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

struct TestResultError: Error {
    init() {}
}
