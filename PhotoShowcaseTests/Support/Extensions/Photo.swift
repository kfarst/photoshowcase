//
//  Photo.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 2/3/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation

@testable import PhotoShowcase

extension Photo {
    init() {
        albumId = 0
        id = 0
        title = ""
        url = ""
        thumbnailUrl = ""
    }
}
