//
//  PhotoListWorkerSpec.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Quick
import Nimble
import Foundation

@testable import PhotoShowcase

class PhotoListWorkerSpec: QuickSpec {
    override func spec() {
        describe("Worker: Photo List") {
            var worker: PhotoListWorker?
            var client: PhotoApiClientMock?
            
            beforeEach {
                client = PhotoApiClientMock()
                worker = PhotoListWorker(apiClient: client!)
            }
            
            describe("#getPhotos") {
                it("calls the photos API") {
                    worker?.getPhotos(page: 1, limit: 0, {_ in })
                    expect(client?.getCalled).to(beTrue())
                }
            }
        }
    }
}
