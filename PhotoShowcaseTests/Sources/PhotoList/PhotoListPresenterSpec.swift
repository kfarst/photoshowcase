//
//  PhotoListPresenterSpec.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import PhotoShowcase

class PhotoListPresenterSpec: QuickSpec {
    override func spec() {
        describe("Presenter: Photo List") {
            var presenter: PhotoListPresenter?
            var viewController: PhotoListViewControllerMock?
            
            beforeEach {
                presenter = PhotoListPresenter()
                viewController = PhotoListViewControllerMock()
                presenter?.viewController = viewController
            }
            
            describe("#showPhotoList") {
                it("renders the photo list in the view") {
                    let response = PhotoList.Fetch.Response(photos: [])
                    presenter?.showPhotoList(response: response)
                    expect(viewController?.renderPhotosListCalled).to(beTrue())
                }
            }
            
            describe("#showThumbnailImage") {
                it("renders the thumbnail image") {
                   let response = PhotoList.Thumbnail.Response(image: UIImage(), cell: UITableViewCell())
                    presenter?.showThumbnailImage(response: response)
                    expect(viewController?.renderThumbnailImageCalled).to(beTrue())
                }
            }
            
            describe("#showError") {
                it("shows an error message") {
                    presenter?.showError(withMessage: "Test error")
                    expect(viewController?.showErrorCalled).to(beTrue())
                }
            }
        }
    }
}
