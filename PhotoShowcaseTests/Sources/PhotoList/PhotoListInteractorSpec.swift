//
//  PhotoListInteractorSpec.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 1/31/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import PhotoShowcase

class PhotoListInteractorSpec: QuickSpec {
    override func spec() {
        describe("Interactor: Photo List") {
            var interactor: PhotoListInteractor?
            var presenter: PhotoListPresenterMock?
            
            beforeEach {
                interactor = PhotoListInteractor(worker: PhotoListWorkerMock())
                presenter = PhotoListPresenterMock()
                interactor?.presenter = presenter
            }
            
            describe("#loadPhotosList") {
                it("presents the photo list after fetch") {
                    let request = PhotoList.Fetch.Request(page: 1, limit: 0)
                    interactor?.loadPhotosList(request: request)
                    expect(presenter?.showPhotoListCalled).to(beTrue())
                }
            }
            
            describe("#loadPhotoThumbnail") {
                it("loads the photo thumbnail") {
                    let request = PhotoList.Thumbnail.Request(url: "htts://my.image", cell: PhotoListTableViewCell())
                    interactor?.loadPhotoThumbnail(request: request)
                    expect(presenter?.showThumbnailImageCalled).to(beTrue())
                }
            }
        }
    }
}
