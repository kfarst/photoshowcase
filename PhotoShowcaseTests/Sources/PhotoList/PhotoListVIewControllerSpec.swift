//
//  PhotoListVIewControllerSpec.swift
//  PhotoShowcaseTests
//
//  Created by Kevin Farst on 2/1/19.
//  Copyright © 2019 Kevin Farst. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import PhotoShowcase

class PhotoListViewControllerSpec: QuickSpec {
    override func spec() {
        describe("View Controller: Photo List") {
            var interactor: PhotoListInteractorMock?
            var viewController: PhotoListViewController?
            
            beforeEach {                
                viewController = PhotoListViewController()
                interactor = PhotoListInteractorMock()
                viewController?.interactor = interactor
            }
            
            it("sets the view state to loading") {
                expect(viewController?.viewState).to(equal(.loading))
            }
            
            describe("#viewDidLoad") {
                it("attempts to fetch the photo list") {
                    viewController?.viewDidLoad()
                    expect(interactor?.loadPhotosListCalled).to(beTrue())
                }
            }
            
            describe("#renderPhotosList") {
                beforeEach {
                    // Clear the photos out each time
                    let viewModel = PhotoList.Fetch.ViewModel(photos: [])
                    viewController?.renderPhotosList(viewModel: viewModel)
                }
                
                describe("with no photos to render") {
                    it("does not update the list of photos") {
                        expect(viewController?.photos.isEmpty).to(beTrue())
                    }
                    
                    it("sets the view state to empty") {
                        expect(viewController?.viewState).to(equal(.empty))
                    }
                }
                
                describe("with photos to render") {
                    beforeEach {
                        let viewModel = PhotoList.Fetch.ViewModel(photos: [Photo()])
                        viewController?.renderPhotosList(viewModel: viewModel)
                    }
                    
                    it("updates the list of photos") {
                        expect(viewController?.photos.count).to(equal(1))
                    }
                    
                    it("sets the view state to loaded") {
                        expect(viewController?.viewState).to(equal(.loaded))
                    }
                }
            }
            
            describe("#renderThumbnailImage") {
                beforeEach {
                    // Clear the photos out each time
                    var viewModel = PhotoList.Fetch.ViewModel(photos: [])
                    viewController?.renderPhotosList(viewModel: viewModel)
                    
                    viewModel = PhotoList.Fetch.ViewModel(photos: [Photo()])
                    viewController?.renderPhotosList(viewModel: viewModel)
                }
                
                it("updates the correct cell with the correct image") {
                    let image = UIImage()
                    
                    let cell = viewController?.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! PhotoListTableViewCell
                    let viewModel = PhotoList.Thumbnail.ViewModel(image: image, cell: cell)
                    
                    viewController?.renderThumbnailImage(viewModel: viewModel)
                    
                    expect(cell.thumbnailImage).to(equal(image))
                }
            }
        }
    }
}
