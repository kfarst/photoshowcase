# Photo Showcase

Display photo thumbnails and photo information in a table format.

## Walkthroughs

#### Photos Loaded

![Photos Loaded](https://imgur.com/XrRzMT1.gif)

#### No Photos Loaded

 ![No Photos Loaded](https://imgur.com/6AxZB1T.gif)

#### Photos Reloaded

![Photos Reloaded](https://imgur.com/coeSphI.gif)

#### Error

https://imgur.com/yvj2FTM.gif

## Features

* Pull to refresh
* Unit tests
* UI tests
* Asynchronous image loading and caching
* Intelligent table view updates
* Data prefetching and pagination
* Error state handling
* Empty state handling
* Package management with Cocoapods

## Cocoapods

* [SwiftIcons](https://cocoapods.org/pods/SwiftIcons)
* [Quick](https://cocoapods.org/pods/Quick)
* [Nimble](https://cocoapods.org/pods/Nimble)
* [OHHTTPStubs](https://cocoapods.org/pods/OHHTTPStubs)